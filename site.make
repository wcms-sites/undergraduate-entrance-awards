core = 7.x
api = 2

; uw_ct_awards_entrance_override
projects[uw_ct_awards_entrance_override][type] = "module"
projects[uw_ct_awards_entrance_override][download][type] = "git"
projects[uw_ct_awards_entrance_override][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_ct_awards_entrance_override.git"
projects[uw_ct_awards_entrance_override][download][tag] = "7.x-1.1"
projects[uw_ct_awards_entrance_override][subdir] = ""

